<!DOCTYPE html>

<html lang="en" lang="en">

<head>
  <meta name="Description" content="Dynamic Optimization. Speculative Execution. Compiler. Automated Parallelization." />
  <meta name="Keywords" content="compiler, optimization, parallelization, automatic, speculative, polyhedral" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="Distribution" content="Global" />
  <meta name="Author" content="Apollo Crew" />
  <meta name="Created" content="20160615">

  <link rel="stylesheet" href="style/apollo.css" type="text/css" />
  <title>APOLLO Automatic speculative POLyhedral Loop Optimizer</title>	
</head>

<body>

<!-- Wrap -->
<div id="wrap">
  <div id="header">				
    <h1 id="logo"><img src="images/apollo_logo.png" width="103" height="80" alt="Apollo Logo"></img></h1>
    <h1 id="title">APOLLO</h1>	
    <h2 id="slogan">Automatic speculative POLyhedral Loop Optimizer</h2>

    <!-- Menu Tabs -->
    <ul>
    <li ><a href="about"><span>About</span></a></li>
    <li ><a href="framework"><span>APOLLO Framework</span></a></li>
    <li ><a href="documentation"><span>Documentation</span></a></li>
    <li ><a href="examples"><span>Examples</span></a></li>
    <li id="current"><a href="download"><span>Download</span></a></li>			
    <li ><a href="authors"><span>Authors</span></a></li> 
    </ul>	
  </div>

<!-- Content-Wrap -->
<div id="content-wrap">		
  <div id="main">	
    <div class="license_section">License</div>
     <p align="left">
      APOLLO is distributed under 
      <a href="docs/LICENSE.txt" target="_blank">The BSD 3-Clause Open Source License</a>.
     </p>
    <div class="git_access_section">git Access</div>
    <p align="left">
     To get APOLLO releases from git, please run the following commands:
     <div class="git_clone_section">
       git clone https://gitlab.inria.fr/pclauss/apollo.git<br>
       git checkout release_number #(e.g., 1.1.1)
     </div>
    </p>
    <p align="left">
     If you would like access to the latest version of APOLLO development, please run the following git command:
     <div class="git_clone_section"> https://gitlab.inria.fr/pclauss/apollo.git</div>
    </p>
    <p>
    <p align="left">
     APOLLO benchmarks releases are also available from git:
     <div class="git_clone_section">
       git clone https://gitlab.inria.fr/pclauss/apollo-benchs.git<br>
       git checkout release_number #(e.g., 1.1.0)
     </div>
    </p>
    <!-- Download Table -->
    <div class="download_section">Download</div>
    <table style="margin: 0.6em; width: 40em" id="download" cellpadding=3px cellspacing=4px>
    <tr>
      <th>APOLLO</th>
      <th>Version</th>
      <th>Date</th>
      <th>Release-Notes</th>
    </tr>
    <tr>
      <td><a href="apollo_packages/apollo-1.2.0.tar.gz">apollo-1.2.0.tar.gz</a></td>
      <td> 1.2.0 </td>
      <td> 8 July 2019 </td>
      <td> <a href="docs/release-notes/release-notes-1.2.0.txt">Notes 1.2.0</td>
    </tr>
    <tr>
      <td>apollo-1.1.1.tar.gz</td>
      <td> 1.1.1 </td>
      <td> 17 November 2017 </td>
      <td> <a href="docs/release-notes/release-notes-1.1.1.txt">Notes 1.1.1</td>
    </tr>
    <tr>
      <td>apollo-1.1.0.tar.gz</td>
      <td> 1.1.0 </td>
      <td> 29 January 2017 </td>
      <td> <a href="docs/release-notes/release-notes-1.1.0.txt">Notes 1.1.0</td>
    </tr>
    <tr>
      <td>apollo-1.0.1.tar.gz</td>
      <td> 1.0.1 </td>
      <td> 16 October 2016 </td>
      <td> <a href="docs/release-notes/release-notes-1.0.1.txt">Notes 1.0.1</td>
    </tr>
    <tr>
      <td>apollo-1.0.tar.gz</td>
      <td> 1.0 </td>
      <td> 1 September 2016 </td>
      <td> <a href="docs/release-notes/release-notes-1.0.txt">Notes 1.0</td>
    </tr>
    <tr></tr>
    <tr></tr>
    <tr>
      <th>APOLLO Benchmarks</th>
      <th>Version</th>
      <th>Date</th>
      <th>Release-Notes</th>
    </tr>
    <tr>
      <td><a href="apollo_packages/apollo-benchs-1.0.0.tar.gz">apollo-benchs-1.0.0.tar.gz</a></td>
      <td> 1.0.0 </td>
      <td> 7 February 2017 </td>
      <td> <a href="docs/release-notes-benchs/release-notes-1.0.0.txt">Notes 1.0.0</td>
    </tr>
    </table>
    </p>
    <br/>
    <br/>
  </div>			
</div>

<!-- Footer -->	
<div id="footer">
  <div class="footer-content">
    <p class="align-center">			
    &copy; 2016 <a href="http://www.inria.fr" target="_blank">INRIA</a> | <a href="http://www.cnrs.fr" target="_blank">CNRS</a> | 
                <a href="http://www.unistra.fr" target="_blank">University of Strasbourg</a>
    </p>		
  </div>
</div>

<!-- End of Wrap -->

</div>
</body>

</html>

