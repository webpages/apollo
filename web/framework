<!DOCTYPE html>

<html lang="en" lang="en">

<head>
  <meta name="Description" content="Dynamic Optimization. Speculative Execution. Compiler. Automated Parallelization." />
  <meta name="Keywords" content="compiler, optimization, parallelization, automatic, speculative, polyhedral" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="Distribution" content="Global" />
  <meta name="Author" content="Apollo Crew" />
  <meta name="Created" content="20160615">

  <link rel="stylesheet" href="style/apollo.css" type="text/css" />
  <title>APOLLO Automatic speculative POLyhedral Loop Optimizer</title>	
</head>

<body>

<!-- Wrap -->
<div id="wrap">
  <div id="header">				
    <h1 id="logo"><img src="images/apollo_logo.png" width="103" height="80" alt="Apollo Logo"></img></h1>
    <h1 id="title">APOLLO</h1>	
    <h2 id="slogan">Automatic speculative POLyhedral Loop Optimizer</h2>

    <!-- Menu Tabs -->
    <ul>
    <li ><a href="about"><span>About</span></a></li>
    <li id="current"><a href="framework"><span>APOLLO Framework</span></a></li>
    <li ><a href="documentation"><span>Documentation</span></a></li>
    <li ><a href="examples"><span>Examples</span></a></li>
    <li ><a href="download"><span>Download</span></a></li>			
    <li ><a href="authors"><span>Authors</span></a></li> 
    </ul>	
  </div>

<!-- Content-Wrap -->
<div id="content-wrap">		
<!-- <img src="images/apollo11.jpg" width="820" height="120" alt="headerphoto" class="no-border" /> -->
  <div id="main">	
    <h1>Overview</h1>
    <p>
     To use APOLLO, the programmer has to enclose the targeted loop nests using a dedicated <b>#pragma</b> directive. 
     Inside the <b>#pragma</b> any kind of loops are allowed, for-loops, while-loops, do-while-loops or goto-loops. 
     Please see <b><a href="docs/ApolloUserGuide.pdf" target="_blank">APOLLO User Guide</a></b> for more details. <br/> <br/>
     <center> <img src="images/example.jpg" alt="code example" align="middle"> </center> <br/> <br/>
     </p>
     <p>
     At compile time, the programmer compiles his annotated source code with APOLLO. Then, the runtime system of APOLLO 
     orchestrates the execution and dynamically optimizes the code. <br/><br/><br/><br/>
   
     <center> <img src="images/overview.png" alt="overview example" style="width:640px; height:128px;"> </center> <br/> <br/>
    </p>

    <h1>Execution by "Chunks"</h1>
    <p>
    To be reactive to changes of its behavior, APOLLO executes the target loop nest by chunks of iterations, switching 
    between code versions at each chunk. A chunk is a set of contiguous iterations of the outermost loop. <br/>

    At the beginning of the execution of a loop, APOLLO profiles a few iterations inside a small chunk using the 
    instrumented version of the code. The next chunk can be executed using a different code version, as for example an 
    optimized version. In this way, the loop is executed as a succession of chunks. Between chunks, the control returns 
    to the runtime system. At this point, APOLLO is able to decide about the execution of the next chunk. In our approach, 
    the chunks are executed sequentially, but iterations inside a chunk may be executed in parallel. <br/> <br/> <br/>
    <center> <img src="images/chunking.png" alt="chunking example" style="width:640px; height:128px;"> </center> <br/> <br/>
    </p>
    <p>
    The behavior of the current chunk and the result of its execution determine the next chunk.
    <ul>
      <li>
      At the beginning of the execution of a loop nest, or after completion of a chunk running the original version of the code, 
      a <b>profiling</b> chunk is launched to capture the runtime code behavior. Once the profiled execution finishes, 
      APOLLO moves to the code-generation phase.
      </li>
      <li>
      During the <b>code generation</b> phase, a prediction model is build and a transformation is selected. From the selected 
      transformation, optimized binary code is generated using the LLVM Just-In-Time compiler. If successful, APOLLO is ready to 
      execute optimized code; if not, the execution continues using the <b>original</b> version of the code. In parallel to this, 
      a background thread executes the original version of the code, in order to mask, at least partially, the related time overhead.
      </li>
      <li>
      Always before executing optimized code, APOLLO performs a preventive <b>backup</b> of the predicted write memory areas 
      (thanks to the prediction model).
      </li>
      Once the backup has been performed, APOLLO executes the <b>optimized</b> code. Optimized chunks are executed until a 
      misspeculation occurs. Iterations inside an optimized chunk are most often run in parallel.
      <li>
      When a misspeculation is detected, a <b>rollback</b> is performed to recover the memory state prior to the execution 
      of the faulty chunk.
      </li>
      <li>
      The same chunk is re-executed using the <b>original</b> version of the code, which is obviously semantically correct.
      </li>
    </ul>
    </p>

    <br/>
    <h1>Results</h1>
    <p>
    The set of benchmarks has been built from a collection of benchmark suites, such
    that the selected codes include a main kernel loop nest and highlight APOLLO's capabilities: 
    <font face="Monospace">SOR</font> from the <a href="http://math.nist.gov/scimark2/" target="_blank">Scimark suite</a>; 
    <font face="Monospace">Backprop</font> and <font face="Monospace">Needle</font> from the 
    <a href="http://www.cs.virginia.edu/~skadron/wiki/rodinia/index.php/Rodinia:Accelerating_Compute-Intensive_Applications_with_Accelerators" target="_blank">Rodinia suite</a>; 
    <font face="Monospace">DMatmat</font>, <font face="Monospace">ISPMatmat</font>, <font face="Monospace">Spmatmat</font> and 
    <font face="Monospace">Pcg</font> from the <a href="https://arxiv.org/abs/0805.3897" target="_blank">SPARK00 suite</a> of irregular
    codes; and <font face="Monospace">Mri-q</font> from the <a href="http://impact.crhc.illinois.edu/parboil/parboil.aspx" target="_blank">Parboil suite</a>. 
    <br/> In table below, we identify the characteristics for each benchmark that make them impossible to parallelize at compile-time.
    </p>
    <br/>
    <p>
    <table align="center" style="width:640px; border:1px solid black; border-collapse:collapse;">
      <tr>
        <th style="border:1px solid black;">Benchmark</th>
        <th style="border:1px solid black;">Has indirections</th>
        <th style="border:1px solid black;">Has pointers</th>
        <th style="border:1px solid black;">Unpredictable bounds</th>
        <th style="border:1px solid black;">Unpredictable scalars</th>
      </tr>
      <tr>
        <td style="border:1px solid black;">Mri-q</td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;"></span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;">&#10003;</span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;"></span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;"></span> </td>
      </tr>
      <tr>
        <td style="border:1px solid black;">Needle</td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;"></span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;">&#10003;</span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;"></span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;"></span> </td>
      </tr>
      <tr>
        <td style="border:1px solid black;">SOR</td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;">&#10003;</span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;">&#10003;</span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;"></span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;"></span> </td>
      </tr>
      <tr>
        <td style="border:1px solid black;">Backprop</td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;">&#10003;</span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;">&#10003;</span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;"></span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;"></span> </td>
      </tr>
      <tr>
        <td style="border:1px solid black;">PCG</td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;">&#10003;</span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;">&#10003;</span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;">&#10003;</span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;">&#10003;</span> </td>
      </tr>
      <tr>
        <td style="border:1px solid black;">DMatmat</td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;">&#10003;</span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;">&#10003;</span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;"></span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;"></span> </td>
      </tr>
      <tr>
        <td style="border:1px solid black;">ISPMatmat</td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;">&#10003;</span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;">&#10003;</span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;">&#10003;</span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;">&#10003;</span> </td>
      </tr>
      <tr>
        <td style="border:1px solid black;">SPMatmat</td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;">&#10003;</span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;">&#10003;</span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;">&#10003;</span> </td>
        <td style="border:1px solid black;"> <span style="font-size: 150%;">&#10003;</span> </td>
      </tr>
    </table>
    </p>
    <br/>   
    <br/>
    <p>
    The benchmark codes were run on two different machines: <br/>
    <ul>
      <li>
       <b>Lemans</b>, a general-purpose multi-core server, with two AMD Opteron 6172 processors of 12 cores each (24 in total).
      </li>
      <li>
      <b>Armonique</b>, an embedded system multi-core chip, with one ARM Cortex A53 64-bit processor of 8 cores.
      </li>
    </ul>
    </p>
    <br/>
    <p>
    We compared the original code optimized with APOLLO (version 1.0) against the best outcome obtained from the original code compiled using 
    either LLVM/Clang (version 3.8) or GCC (version 5.3.1) with optimization level 3 (-O3).
    </p>
    <br/>
    <p>
    <b>AMD Opteron 6172, 2X12-core</b> <br/> <br/>
    <center> <img src="images/amd.png" alt="AMD result" style="width:768px;"> </center> <br/> <br/>
    </p>
    <p>
    <br/> <br/>
    <b>ARM Cortex A53, 8-core</b>
    <center> <img src="images/arm.png" alt="ARM result" style="width:768px;"> </center> <br/> <br/>
    </p>
    <br/>
    <br/>
  </div>			
</div>

<!-- Footer -->	
<div id="footer">
  <div class="footer-content">
    <p class="align-center">			
    &copy; 2016 <a href="http://www.inria.fr" target="_blank">INRIA</a> | <a href="http://www.cnrs.fr" target="_blank">CNRS</a> | 
                <a href="http://www.unistra.fr" target="_blank">University of Strasbourg</a>
    </p>		
  </div>
</div>

<!-- End of Wrap -->

</div>
</body>

</html>

