# Apollo 1.0.1 - 16 October 2016

## Bug fixes

- Don't launch  code generation  phase when  memory accesses  or loops
  were not executed

## Improvements

- Avoid multiple useless retry for optimized chunks resulting in early
missprediction.
