//===--- matrixmul.c ------------------------------------------------------===//
//
// APOLLO matrix multiplication example
//
//===---------------------------------------------------------------------===//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* Initialize matrix: function prototype. */
void initializeMatrix(double **Matrix, const int Row, const int Col);

/* MAIN */
int main()
{
  /* Matrix A */
  const int RowA = 1000;
  const int ColA = 1000;
  double **Matrix_A = (double**)malloc(RowA * sizeof(double*));
  for (int i = 0; i < RowA; i++) {
    Matrix_A[i] = (double*)malloc(ColA * sizeof(double));
  }

  /* Matrix B */
  const int RowB = 1000; /* should be the same size as ColA */
  const int ColB = 1000;
  double **Matrix_B = (double**)malloc(RowB * sizeof(double*));
  for (int i = 0; i < RowB; i++)
    Matrix_B[i] = (double*)malloc(ColB * sizeof(double));

  /* Matrix C */
  double **Matrix_C = (double**)malloc(RowA * sizeof(double*));
  for (int i = 0; i < RowA; i++) {
    Matrix_C[i] = (double*)malloc(ColB * sizeof(double));
  }

  /* Initialize Matrix_A and Matrix_B */
  initializeMatrix(Matrix_A, RowA, ColA);
  initializeMatrix(Matrix_B, RowB, ColB);

  printf("Start Matrix Multiplication\n");
  clock_t start_time = clock();

  //kernel!
#pragma apollo dcop
{
  /* ======== Matrix multiplication =====//===== C = A * B ============= */
  for (int i = 0; i < RowA; i++) {
    for (int j = 0; j < ColB; j++) {
      for (int k = 0; k < ColA; k++) {
        Matrix_C[i][j] += Matrix_A[i][k] * Matrix_B[k][j];   
      }
    }
  }
}

  clock_t end_time = clock();
  double total_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;
  printf("End Matrix Multiplication\n");
  printf("Total time taken by CPU: %f seconds\n", total_time);

  /* free Matrix A */
  for (int i = 0; i < RowA; i++) {
    free(Matrix_A[i]);
  }
  free(Matrix_A);

  /* free Matrix B */
  for (int i = 0; i < RowB; i++) {
    free(Matrix_B[i]);
  }
  free(Matrix_B);

  /* free Matrix C */
  for (int i = 0; i < RowA; i++) {
    free(Matrix_C[i]);
  }
  free(Matrix_C);

  return 0;
}

/* Initialize matrix: function implementation. */
void initializeMatrix(double **Matrix, const int Row, const int Col) {
  for (int i = 0; i < Row; i++) {
    for (int j = 0; j < Col; j++) {
      Matrix[i][j] = (double)(rand() % 3);
    }
  }
}

