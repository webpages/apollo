<!DOCTYPE html>

<html lang="en" lang="en">

<head>
  <meta name="Description" content="Dynamic Optimization. Speculative Execution. Compiler. Automated Parallelization." />
  <meta name="Keywords" content="compiler, optimization, parallelization, automatic, speculative, polyhedral" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="Distribution" content="Global" />
  <meta name="Author" content="Apollo Crew" />
  <meta name="Created" content="20160615">

  <link rel="stylesheet" href="style/apollo.css" type="text/css" />
  <title>APOLLO Automatic speculative POLyhedral Loop Optimizer</title>	
</head>

<body>

<!-- Wrap -->
<div id="wrap">
  <div id="header">				
    <h1 id="logo"><img src="images/apollo_logo.png" width="103" height="80" alt="Apollo Logo"></img></h1>
    <h1 id="title">APOLLO</h1>	
    <h2 id="slogan">Automatic speculative POLyhedral Loop Optimizer</h2>

    <!-- Menu Tabs -->
    <ul>
    <li ><a href="about"><span>About</span></a></li>
    <li ><a href="framework"><span>APOLLO Framework</span></a></li>
    <li ><a href="documentation"><span>Documentation</span></a></li>
    <li id="current"><a href="examples"><span>Examples</span></a></li>
    <li ><a href="download"><span>Download</span></a></li>			
    <li ><a href="authors"><span>Authors</span></a></li> 
    </ul>	
  </div>

<!-- Content-Wrap -->
<div id="content-wrap">		
  <div id="main">	
    <a name="Apollo">
    <h1>APOLLO Examples</h1>
    </a>
    <p>
     <br/>
     <b>Example 1:</b> <br>
     A small example (matrix multiplication <a href="example_files/matrixmul.c" download="apolloexample">matrixmul.c</a>) of using APOLLO
     and comparing it with GCC and LLVM/Clang.
     <br/>
     <br/> 
     <iframe width="690px" height="390px" allowfullscreen 
      src="https://www.youtube.com/embed/zaM8va1SPw0">
     </iframe>
     <br/>
     <br/>
     <!-- Download video example: <a href="example_files/apollo_video" download="apollovideoexample">apollo video</a> -->
     <br/> 
     <b>Example 2:</b> <br>
     Sparse matrix multiplication (<a href="https://arxiv.org/abs/0805.3897" target="_blank">SPARK00 suite</a>) with a 2000x2000 square matrix input.
     <br/>
     <br/>
     <iframe width="690px" height="390px" allowfullscreen 
      src="https://www.youtube.com/embed/3i9AkcDk_v8">
     </iframe>
     <br/>
     <br/>
     <!-- Download video example: <a href="example_files/apollo_spmat_video" download="apollovideoexample">apollo video</a> -->
    </p>
    <br />
    <br />
  </div>			
</div>

<!-- Footer -->	
<div id="footer">
  <div class="footer-content">
    <p class="align-center">			
    &copy; 2016 <a href="http://www.inria.fr" target="_blank">INRIA</a> | <a href="http://www.cnrs.fr" target="_blank">CNRS</a> | 
                <a href="http://www.unistra.fr" target="_blank">University of Strasbourg</a>
    </p>		
  </div>
</div>

<!-- End of Wrap -->

</div>
</body>

</html>

